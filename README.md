# express-web-components

2 Web components integrated on same html page, served by Express and Nodejs

npm run dev

Requirements
Server
Nested Web Component
Parent/Child Relationship
Utilize internal styling
Utilize internal storage


Sources:

https://medium.com/@armando_amador/introduction-to-setting-up-local-web-development-with-node-and-express-e3dbb764de67
https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_templates_and_slots
https://www.devbridge.com/articles/a-practical-introduction-to-web-components/
https://www.youtube.com/watch?v=PFpUCnyztJk
https://codepen.io/rokask/pen/KYgJZJ
https://codepen.io/ratchasak/pen/ggxmgX
https://codepen.io/scottfr/pen/OJVoOwe
http://web.simmons.edu/~grabiner/comm244/weekfour/document-tree.html
https://www.cssportal.com/html-table-generator/
https://www.quackit.com/html/html_table_generator.cfm
https://appdividend.com/2022/01/24/javascript-import/

