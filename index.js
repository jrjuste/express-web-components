//import the express module and call it as a function
const express = require('express');
const app = express();
const PORT = 3000;

app.use(express.static('src'));

//start up the server on port3000
app.listen(PORT, function(){
    console.log(`Server URL: http://localhost:${PORT}`);
});