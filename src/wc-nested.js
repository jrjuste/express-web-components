export default customElements.define('wc-nested',
  class extends HTMLElement {
    constructor() {
      super();

      //grab my template and content
      const template = document.getElementById('nested-template');
      const templateContent = template.content;

      //open the shadown dowm
      const shadowRoot = this.attachShadow({mode: 'open'});

      //create the style for my template content
      const style = document.createElement('style');
      style.textContent = `
        div { padding: 10px; border: 1px solid gray; width: 200px; margin: 10px; }
        h2 { margin: 0 0 10px; color: green;}
        ul { margin: 0; color: brown; font-weight: bold; }
        p { margin: 10px 0; }
      `;
      //append the style to the content and template
      shadowRoot.appendChild(style);

      //clone node
      shadowRoot.appendChild(templateContent.cloneNode(true));
  }
});