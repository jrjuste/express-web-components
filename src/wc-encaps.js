//creating a custom element to display encapsulation of styles
export default class StyleEncaps extends HTMLElement {
    constructor() {
      super();
     this.encaps = this.attachShadow({ mode: 'open' }); // Creates a shadow DOM root node for the element

     //this content will appear in the table
     this.encaps.innerHTML = `
     <p><input value="Styled in Web Component" style="background-color: green; font-size: 13px;
     font-weight: bold;"></p>`;
    }
  }


  customElements.define('wc-encaps', StyleEncaps);
