export default class SlottedContent extends HTMLElement {
    constructor() {
      super();
      //grab the id and teplate content
      let template = document.getElementById('wc-slot');
      let templateContent = template.content;

      //open the dom and clone the node
      const shadowRoot = this.attachShadow({mode: 'open'});
      shadowRoot.appendChild(templateContent.cloneNode(true));
    }
 }

 customElements.define('wc-slot', SlottedContent);
